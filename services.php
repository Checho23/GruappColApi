<?php 
require 'vendor/autoload.php';

require ('credenciales.php');

require ("ConsultaCostos.php");

/**
* 
*/
class Services 
{
	//Variables globales! 
	protected $menortodas=0;
	protected $idmenor=0;
	protected $idcliente="";
	protected $menorlongitud = "";
	protected $menordistancia = 999999999999999999999;
	protected $menorlatitud = "";
	protected $clienteOrigenLongitud ="";
	protected $clienteOrigenLatitud ="";
	protected $clienteDestinoLongitud ="";
	protected $clienteDestinoLatitud ="";
	protected $coorCliente = "";
	protected $CostoOrigen="";
	protected $CostoDestino="";
	protected $Categoria="";
	protected $TipoAccidente="";
	protected $durationOrigen="";
	protected $durationDestino="";


	
	function __construct()
	{

		$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
		$solicitud = $firebase->get('solicitud_servicio/cliente');

		$solicitudes = (array) json_decode($solicitud);


		foreach ($solicitudes as $key => $value) 
		{
			if($value->estado == "nuevo" || $value->estado =="Sin respuesta")
			{
				$this->Reiniciar();
				$this->clienteOrigenLongitud =$value->origen[0];

		        $this->clienteOrigenLatitud = $value->origen[1];
		        $this->clienteDestinoLongitud =$value->destino[0];
		        $this->clienteDestinoLatitud = $value->destino[1];
		        $this->Categoria = $value->Categoria;
		        $this->idcliente = $key; //Almacenamiento de ID de cliente

		        $no = [];
		        if(isset($value->TipoAccidente))
		        {
		        	$this->TipoAccidente = $value->TipoAccidente;
		        }
		        else
		        {
		        	$this->TipoAccidente="No posee";
		        }
		        if(isset($value->no))
		        {
		        	foreach ($value->no as $key => $value) 
		        	{
		        		array_push($no,$value);
		        	}
		        }
		        //Consulta de todos los usuarios 
				$grueros = $firebase->get('/driver');
				$todosGrueros = (array) json_decode($grueros);

				foreach ($todosGrueros as $id => $valor) 
				{
					$esta = false;
					foreach ($no as $idNo) 
					{
						if($id == $idNo)
						{
							$esta = true;
						}
					}
					if(!$esta)
					{
						if($valor->estado !="offline")
						{
							$thisLongitude  = $valor->location[0];
							$thisLatitude = $valor->location[1];

							if($thisLongitude!= null && $thisLatitude !=  null)
							{

								$url ='https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->clienteOrigenLongitud.','.$this->clienteOrigenLatitud.'&destinations='.$thisLongitude.'%2C'.$thisLatitude.'&key=AIzaSyCSobZVkcFYcV1sLHKKItWj4xr_pK4rJ9Y'; 
						   
					 	     
							    $ch = curl_init($url); 
							    // Configuring curl options 
							    $options = array( 
							        CURLOPT_RETURNTRANSFER => true,     
							    CURLOPT_HTTPHEADER => array('Accept: application/json'), 
							    CURLOPT_SSL_VERIFYPEER => false,     
							    ); 
							    // Setting curl options 
							    curl_setopt_array( $ch, $options );     
							    // Getting results 
							    $response = curl_exec($ch); // Getting jSON result string   
							    // Cerrar el recurso cURL y liberar recursos del sistema 
							    curl_close($ch);   

							    $data = json_decode($response, true); 
							    $this->durationOrigen = ($data['rows'][0]['elements'][0]['duration']['text']);

							 	$metros = ($data['rows'][0]['elements'][0]['distance']['value']);

							 	if($metros < $this->menordistancia)
				                  {
				                    $this->menordistancia = $metros;
				                    $idusermenor = $id;
				                    $this->idmenor = $id;

				                    $this->asignarDistancia($this->menordistancia,$idusermenor,$thisLongitude,$thisLatitude);
				                  }
				                 
							 	// echo "Distancia del ID ".$id." hasta el origen ". $metros.'<br>';
							}
						}//end if
					}//End If de estar o no estar (Ser o no ser.. xD)
				}
				//Finalización de foreach En la que se consultan usuarios
				$superVariableSalvadora = $this->idmenor;//Almacenamiento de id de usuario
				if($superVariableSalvadora != "")
				{
					$urlDistanciaFinal ='https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$this->clienteOrigenLongitud.','.$this->clienteOrigenLatitud.'&destinations='.$this->clienteDestinoLongitud.'%2C'.$this->clienteDestinoLatitud.'&key=AIzaSyCSobZVkcFYcV1sLHKKItWj4xr_pK4rJ9Y'; 
					   
				    $distanciaOrigenDestino = curl_init($urlDistanciaFinal); 
				    // Configuring curl options 
				    $options = array( 
				        CURLOPT_RETURNTRANSFER => true,     
				    CURLOPT_HTTPHEADER => array('Accept: application/json'), 
				    CURLOPT_SSL_VERIFYPEER => false,     
				    ); 
				    // Setting curl options 
				    curl_setopt_array( $distanciaOrigenDestino, $options );     
				    // Getting results 
				    $response = curl_exec($distanciaOrigenDestino); // Getting jSON result string   
				    // Cerrar el recurso cURL y liberar recursos del sistema 
				    curl_close($distanciaOrigenDestino);   

				    $data = json_decode($response, true);     
				 	$metros = ($data['rows'][0]['elements'][0]['distance']['value']);
					$this->durationDestino = ($data['rows'][0]['elements'][0]['duration']['text']);

				 	// echo($this->menordistancia."<br>");
				 	// echo($metros."<br>");

				 	$costos = new ConsultaCostos();

				 	$menordistanciaKm = $this->menordistancia/1000;
				 	$this->CostoOrigen = $costos->getCostos($this->Categoria,$menordistanciaKm,'Origen');

				 	$distanciaKm = $metros/1000;

				 	$this->CostoDestino = $costos->getCostos($this->Categoria,$distanciaKm,'Destino');


				 	// echo($this->CostoDestino['costoFormat']);

						$this->hacerRegistro();
				}

				if($superVariableSalvadora == "")
				{
					$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);
					$data = array(
						'estado'=> 'Sin respuesta'
						);
					$firebase->update('solicitud_servicio/cliente/'.$this->idcliente, $data); 
				}
			}
			// echo "<pre>";
			// print($key);
			// echo "<br>";
			// print_r($value);
			// echo "</pre>";
		}
	}// FIN DEL CONSTRUCTOR



	public function asignarDistancia($valor,$id,$longitud,$latitud)
	{

	  $this->menortodas = $valor;
	  $this->idmenor = $id;
	  $this->menorlongitud = $longitud;
	  $this->menorlatitud = $latitud;
	}

	public function hacerRegistro()
	{
		$firebase = new \Firebase\FirebaseLib(DEFAULT_URL, DEFAULT_TOKEN);

		$grueros = $firebase->get('driver/'.$this->idmenor);
		$grueroSeleccionado = (array) json_decode($grueros);

		// print_r($grueroSeleccionado['userinfo']->ciudad);

		$driver = $grueroSeleccionado['userinfo'];
		// $usuario = new ConsultaCostos();
		// $driver = $usuario->getDriver($this->idmenor);


		$vehicle = $grueroSeleccionado['carinfo'];


		$cliente = $firebase->get('user/'.$this->idcliente);
		$cliente = (array) json_decode($cliente);
		
// 		print_r($cliente['userinfo']);
// return false;
		$nombreCliente =$cliente['userinfo']->name;
		$telefonoCliente =$cliente['userinfo']->phone;

		$costoTotal = $this->CostoOrigen['costoNormal']+$this->CostoDestino['costoNormal'];
		$data = array(
			'estado'=> 'ok'
			);
		$codigo = rand(1, 9999);
// $firebase->update('solicitud_servicio/cliente/'.$this->idcliente, $data); 
		$registro = array(
			 $this->idmenor => array(
					    "idGruero" => $this->idmenor,
					    "LongitudOrigen" => $this->clienteOrigenLongitud,
					    "LatitudOrigen" => $this->clienteOrigenLatitud,
					    "LongitudDestino" => $this->clienteDestinoLongitud,
					    "LatitudDestino" => $this->clienteDestinoLatitud,
					    "Metros" => $this->menordistancia,
					    "CostoOrigen" => $this->CostoOrigen['costoNormal'],
					    "CostoDestino" => $this->CostoDestino['costoNormal'],
					    "Categoria"=>$this->Categoria,
					    "total" =>$this->CostoOrigen['costoNormal']+$this->CostoDestino['costoNormal'],
					    // "CostoDestino" => $this->CostoDestino['costoFormat'],
					    "idCliente" =>$this->idcliente,
					    "Tipo_Accidente" =>$this->TipoAccidente,
					    "codigo" => $codigo,
					    "Nombre"=>$driver->name." ".$driver->last_name,
					    "placas"=>$vehicle->plate,
					    // "foto"=>$driver[0]['avatar'],
					    "telefono"=>$telefonoCliente,
					    "nombreCliente"=>$nombreCliente,
					    "TiempoOrigen"=>$this->durationOrigen,
					    "TiempoDestino"=>$this->durationDestino)
		);

		// $firebase->set('solicitud_servicio/gruero/'.$this->idcliente, $registro);
$firebase->update('/respuesta/', $registro);
		// $firebase->set('respuesta/'.$codigo."-".$this->idmenor, $registro);
		// echo("Vamos bien no mames");
		$estadoGruero = array('estado'=>'offline');
$firebase->update('/driver/'.$this->idmenor.'/', $estadoGruero);
	}


	public function Reiniciar()
	{		

		$this->menortodas=0;
		$this->idmenor=0;
		$this->idcliente="";
		$this->menorlongitud = "";
		$this->menordistancia = 999999999999999999999;
		$this->menorlatitud = "";
		$this->clienteOrigenLongitud ="";
		$this->clienteOrigenLatitud ="";
		$this->clienteDestinoLongitud ="";
		$this->clienteDestinoLatitud ="";
		$this->coorCliente = "";
		$this->CostoOrigen="";
		$this->CostoDestino="";
		$this->Categoria="";
		$this->TipoAccidente="";
		$this->durationOrigen="";
		$this->durationDestino="";

	}
}



/*
CALCULO DE DISTANCIA :v

		 $url ='https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=AIzaSyCSobZVkcFYcV1sLHKKItWj4xr_pK4rJ9Y'; 
	   
 	     
	    $ch = curl_init($url); 
	    // Configuring curl options 
	    $options = array( 
	        CURLOPT_RETURNTRANSFER => true,     
	    CURLOPT_HTTPHEADER => array('Accept: application/json'), 
	    CURLOPT_SSL_VERIFYPEER => false,     
	    ); 
	    // Setting curl options 
	    curl_setopt_array( $ch, $options );     
	    // Getting results 
	    $response = curl_exec($ch); // Getting jSON result string   
	    // Cerrar el recurso cURL y liberar recursos del sistema 
	    curl_close($ch);   

	    $data = json_decode($response, true);     
	 	$metros = ($data['rows'][0]['elements'][0]['distance']['value']);
*/