<?php 


	require ("conexion.php");

	/**
	* Regresa todos los registros
	*/
	class ConsultaCostos extends Conexion
	{
		
		public function ConsultaCostos()
		{
			parent::__construct();//Ejecutar el constructor de la clase padre (conexion)

		}

		public function getCostos($categoria,$distancia,$tipoDistancia)
		{
			$sql = "SELECT * FROM costs WHERE type='".trim($categoria)."' LIMIT 1";

			$sentencia = $this->conexion_db->prepare($sql);

			$sentencia->execute(array());

			$tarifas = $sentencia->fetchAll(PDO::FETCH_ASSOC);

			$sentencia->closeCursor();

			// $sentencia['costo_kilometro_adicional'];

			$tarifaKmAdicional = $tarifas[0]['costo_kilometro_adicional'];

			$CostoMinimo = $tarifas[0]['costo_minimo'];

			if($tipoDistancia =="Origen")
	        {
	            if($distancia>15)  
	            {
	                $distanciaTotal = $distancia-15;
	                $subtotal = $distanciaTotal * $tarifaKmAdicional;
	            }
	            else  $subtotal = 0;
	            
	            $costoNormal = $subtotal;
	            $subtotal = $this->cop_format($subtotal);


	            $coleccion = array(
	                    'costoNormal' => $costoNormal,
	                    'costoFormat' => $subtotal,
	                );

	            return ($coleccion);
	            

	        }
	        else
	        {
	            $costoAdicional = 0;
	            if($distancia>15)
	            {
	                $distanciaTotal = $distancia - 15;
	                $costoAdicional = $distanciaTotal * $tarifaKmAdicional;
	            }
	            else $costoAdicional = 0;

	            $Sumatoria = $CostoMinimo+ $costoAdicional;
	            $subtotal = $this->cop_format($Sumatoria);

	            $coleccion = array(
	                    'costoNormal' => $Sumatoria,
	                    'costoFormat' => $subtotal,
	                );

	            return ($coleccion);

	        }
		}

		public function getDriver($id)
		{  
			$sql = "SELECT * FROM drivers WHERE account='".$id."' LIMIT 1";

			$sentencia = $this->conexion_db->prepare($sql);

			$sentencia->execute(array());

			$driver = $sentencia->fetchAll(PDO::FETCH_ASSOC);

			$sentencia->closeCursor();

			return $driver;
		}

		public function getVehicle($id)
		{  

			$sql = "SELECT * FROM driver_vehicles WHERE driver_id='".$id."'";

			$sentencia = $this->conexion_db->prepare($sql);

			$sentencia->execute(array());

			$vehicle = $sentencia->fetchAll(PDO::FETCH_ASSOC);

			$sentencia->closeCursor();

			return $vehicle;
		}



		public function cop_format($number)
		{
		    $fmt = new NumberFormatter( 'es_CO', NumberFormatter::CURRENCY );
		    $number = ceil($number / 100) * 100;

		   	return 'COP '.$fmt->formatCurrency($number, "COP");
		}

	}




?>